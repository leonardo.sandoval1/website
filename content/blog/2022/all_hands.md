---
Title: "SOAFEE Inaugural Public All Hands"
Description: "The SOAFEE Governing Body are pleased to announce the inaugural All Hands virtual meeting for the SOAFEE Special Interest Group.  Click through to find out more."
Date: "2022-01-14"
Banner:
  Active: false
  Title: Learn about SOAFEE at Public All-hands meeting
  Description: Join the virtual all-hands on January 27th, 7am pacific to hear about SOAFEE, how to start developing with the framework, and how to get involved with this ground-breaking initiative.
  Background: "banner/banner3.jpg"
---

# SOAFEE Public All-Hands Meeting

SOAFEE is an industry-led initiative to provide a framework for cloud-native development for real-time and safety-critical applications at the edge. Based on open-standards, SOAFEE enables business and enterprise companies to offer their technology and products in a standard environment for automotive and industrial developers. SOAFEE also welcomes academic and individual developers to contribute and collaborate with business and enterprise companies as well as consumers of their technology. 

{{< button url="#" text="SOLD OUT">}}

The SOAFEE initiative is managed as a Special Interest Group (SIG) and has a governing body, technical and marketing steering committees and working groups. This structure is responsible for defining the SOAFEE architecture, incorporating existing open-standards, and producing a reference implementation that can incorporate open-source and commercial technologies.   

The SOAFEE governing body is holding a public all-hands virtual meeting on January 27th 2022 at 7am PST, inviting members of business, academic and developer communities to learn more about what value SOAFEE can offer and how they can get involved with this initiative.

## Governing Body Members

{{< imagegrid columns="3" data="members.governing_body" media_page="headless_media" >}}

{{< button url="#" text="SOLD OUT">}}
