---
Title: "soafee.io Development Process"
---
# soafee.io Development process

I use vscode as my editor of choice, the documentation here assume you are using the same.  If you prefer to use a different editor, you may need to adapt how you use this guide.

## Tools needed

- git (https://git-scm.com/download)
- docker (https://docs.docker.com/get-docker/)
- vscode (https://code.visualstudio.com/)

## Before you start

### Upload your public ssh key

If you plan on publishing content, you need to upload a public ssh key to the gitlab server.

- First create a public ssh key using ```ssh-keygen``` or similar if you don't already have one
- Upload the key to https://gitlab.arm.com/-/profile/keys

## Get the website sources

When you open a new vscode window, you will be presented with a screen that looks something like this

![Clone git repo](images/clone_git_repo.png)

Click on the 'Clone Git Repository...', you will be presented with a dialogue asking for the repository source

![Clone git repo source](images/clone_git_repo_source.png)

You should enter the URL of the soafee.io git repository.

```
git@git.gitlab.arm.com:soafee/website.git
```

You will then be asked to specify a location to store the code locally.  Choose something that works for you.  This will initiate cloning the remote repository to your local machine.

Once complete, you will be presented with a dialgue that looks like this

![Open cloned repo](images/open_cloned_repo.png)

Click on 'Open', and the code will now be available in the editor.

## Deploy the website locally

Serve the content using the hugo development server.  You need to have an open terminal to start the server, luckily vscode has the ability to launch a terminal for you.  

Select the vscode menu item 'Terminal->New Terminal'.  This will open a new terminal for you.  You can then use docker to launch the [hugo](https://gohugo.io/) static site generator's development server using the command below.

```bash
# Make sure you are in the directory that contains the website
# 
# Docker options explained:
#  -p maps the container port to the host port.  Hugo defaults to 1313
#  -v maps the current directory to /data in the container
#     m5p3nc3r/hugo is the name of the container to run
# 
# Hugo commandline explained:
#  serve          Creates a development webserver for your content
#  --bind 0.0.0.0 if you want to bind to all network addresses
#                 This will allow you to see the site from a mobile etc
docker run -p 1313:1313 -v $PWD:/data m5p3nc3r/hugo serve --bind 0.0.0.0
```

Which should generate output that looks something like this

```bash
Start building sites … 
hugo v0.88.1-5BC54738 linux/amd64 BuildDate=2021-09-04T09:39:19Z VendorInfo=gohugoio

                   | EN  
-------------------+-----
  Pages            |  7  
  Paginator pages  |  0  
  Non-page files   | 11  
  Static files     |  8  
  Processed images |  8  
  Aliases          |  0  
  Sitemaps         |  1  
  Cleaned          |  0  

Built in 32 ms
Watching for changes in /data/{content,data,static,themes}
Watching for config changes in /data/config.toml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 0.0.0.0)
Press Ctrl+C to stop
```

You should then be able to launch the website at http://localhost:1313.  Hugo will monitor the filesystem for changes and rebuild the site whenever you edit files.  So you should never need to re-run this command again.

The hugo server may crash if a build fails due to badly formed content, this shouldn't affect you so long as you are only working with markdown, but if you notice your content not updating in the the browser, please restart the hugo server.

## Development workflow

All development on the website should happen on a git branch.  The reason for this is that anything published to the 'main' branch will be pushed live to https://soafee.io.  We need to be able to peer review content before it is published, and only maintainers of the website should have permissions to publish to the main branch.

### Create a development branch

First you need to create a development branch to work on, never work directly on the 'main' branch.

At the bottom left of your vscode window, you should see something like this

![vscode bottom left image](images/vscode_bottom_left.png)

the 'main' here says that you are currently on the main branch.  If you click on the branch indicator (main in this instance, if you are already on a different branch, this will be the name of your current development branch), you will be presented with the following

![create branch](images/create_branch.png)

If you select 'Create new branch from', you will be asked to enter the name of the new branch.  Please choose something that represents the work you are about to do, for instance this post is being created on a branch called 'dev_process'.

If you selected ```Create new branch from...```, you will then be aksed what the base branch you want to create your development branch from.  In most instances, you will want to select ```main```, as you want to base your commits on the currently live site.

If all has gone well, you should see the text in the bottom left of the vscode editor now state the branch name that you just created.

### Create a new blog post

All blog posts are contained in the ```content/blog/``` directory.  The structure under this directory is used to group content together.  If you want to create a new blog post, simply create a new directory ```content/blog/<year>/<blog_name>/``` where ```<year>``` is the current year, and ```<blog_name>``` is the name of your blog post.  Note, that the ```blog_name``` will become a part of the URL used to access the content, so make sure it is something sensible and relevant to the post.

Once you have created the directory, you add an ```index.md``` file, which will form the base content for your post.  The file you created will be at the location ```content/blog/<year>/<blog_name>/index.md``` and once created will be available to view at ```http://localhost:1313/blog/<year>/blog_name/```

Hugo uses a structure called 'front matter' to define the metadata for the post.  It needs to be the first lines in the post, and looks something like this:

```
---
Title: "This is the title of your blog post"
Description: "This is the description of your blog post"
date: "2022-01-05"
---
```

The Title and Description will be used to automatically create the card in the page http://localhost:1313/blog.  The date will be used to show when the blog was published.  A link to the actual blog post will be added to the card, so you should be able to click on it now.

If you add the correct front matter to descrive your post, you should be able to go to http://localhost:1313/blog and see your entry in the list.  Note that the entries are ordered by the ```date``` entry in the front matter, so if you want your post to be at the top of the list, make sure you use todays date.

### Add content to the blog post

The rest of the content below the font matter is the actual content of the blog post.  It uses a format known as 'markdown' for formatting the content.  The styling of the page will be managed by the Hugo theme being used to ensure it is consistent with other pages on the site.

Some useful references for starting with markdown are

- [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- Interactive [Markdown tutorial](https://www.markdowntutorial.com/)
- Longform [Markdown guide](https://www.markdownguide.org/)

Vscode has a side-by-side live view of the rendered content, you can open this view by clicking on the highlighted button shown in the image below.

![markdown preview](images/markdown_preview.png)

### Commit the content

Git can be a complex beast, and its full use goes beyond the scope of this document.  This acts as a simple guide to publish content so long as you don't hit any issues (conflicts etc).

You need to commit your changes to your local repository before you can publish them.  The diagram below shows the commit dialgue in vscode.

![Commit dialogue](images/commit_dialogue.png)

1. This is the source control button.  It shows the number of uncommitted changes you currently have.  Clicking on it will open the source control side menu which is shown in the image.
2. ```Staged Changes``` shows the changes that will be committed when you click on the 'commit' button descibed later.
3. ```Changes``` shows the currently unstaged files.  Click on the ```+``` icon to move them to the Staged Changes list.
4. This is the commit message.  It needs to be meaningful, have a single first line (title) followed by a blank line.  The next lines are the description of the commit.  With the last line required to be a 'Signed-off-by' signature as required by the projects DCO.
5. The commit button.  Once you are happy with the staged files and the description, click this button to commit to your local branch.

You can continue to refine the content in your branch, remembering to commit any new changes that you make.

### Push the content

Once you are happy with the content, you can push it to the upstream repository.

![Push branch](images/push_branch.png)

1. Click the Publish Branch button if you have not yet pushed the branch upstream.  This will create the branch on the remote server and push your local commits.
2. If you have already published the branch and want to push additional changes to the branch, use the 'push' item from the context menu as shown.

If you are the only person working on this branch, and are not working on any shared files (which should not be required for a blog post), there should not be any issues with this process.

The content will now be available for review upstream.  Once the review is complete, the website maintainer can merge the content into the 'main' branch which will trigger the content to be published live.

Once the review is complete, the site maintainer can merge the branch you have created into the ```main``` branch which will trigger a site rebuild and publish to live.