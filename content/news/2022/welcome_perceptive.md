---
Title: "Perceptive joins SOAFEE SIG"
Description: "New member announcement – click for more details on Perceptive"
Date: "2022-03-09"
Member: "voting/perceptive"

Banner:
  Active: true
  Title: Perceptive joins SOAFEE SIG
  Description: Perceptive joins the SOAFEE SIG as a voting member.
  Background: "banner/banner2.jpg"

Card:
  Class: new-member
---


{{< member_header >}}
# Perceptive joins SOAFEE SIG

The SOAFEE community are excited to welcome Perceptive as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About Perceptive

Perceptive is building a full-stack sensing and perception platform for autonomous intelligence. Based on groundbreaking innovations on both hardware and software, Perceptive changes fundamentally the way sensing and perception software are developed and implemented in hardware. 

Perceptive brings deep and unique expertise on sensing and perception for autonomy and robotics to SOAFEE. Our domain includes both the physical layer, all the sages of vehicle compute, as well as DSP and software algorithms. We strongly believe that the future of autonomy sensing is software-defined and centered on AI, and we're excited to foster that vision within the SOAFEE architecture.


