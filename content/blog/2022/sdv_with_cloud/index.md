---
Title: Accelerating Software-Defined Vehicles through Cloud-To-Vehicle Edge Environmental Parity
Description: A white paper from AWS and Arm that explores the role of cloud-native DevOps in automotive system development, the role of cloud to edge environmental parity and role of SOAFEE as the technology enabler.
Date: 2022-01-21
Banner:
  Active: true
  Title: Accelerating SDV's through Cloud-To-Vehicle Environmental Parity
  Description: A white paper from AWS and Arm exploring the role of cloud-native DevOps in automotive system development, the role of cloud to edge environmental parity with SOAFEE as the technology enabler
  Background: banner/banner4.jpg
---

# Accelerating Software-Defined Vehicles through Cloud-To-Vehicle Edge Environmental Parity

{{< img src="images/sdv_with_cloud.svg" >}}

In this white paper we look at what it means to apply cloud-native approach to automotive system development, focusing particularly on achieving environmental parity between cloud to edge execution environments.  The paper explores the impact this approach has in accelerating the time to market of software-defined vehicles and the role that SOAFEE plays as key technology enabler.

{{< button url="https://armkeil.blob.core.windows.net/developer/Files/pdf/white-paper/arm-aws-edge-environmental-parity-wp.pdf" text="Read the white paper">}}