---
title: "SOAFEE"
date: 2021-10-18T13:44:27-05:00
draft: false
description: Scalable Open Architecture for Embedded Edge (SOAFEE) is an industry-led collaboration that is bringing the best practices of cloud-native with the requirements for functional safety, security and real-time to the automotive domain.
keywords: Cloud native, Software defined, Cloud native development, Automotive software development, Cloud native technology stack, Automotive software platform
---

# {{< figure class="banner-img" src="/images/ARM1828_SOAFEE_Dark_ST2.svg" alt="SOAFEE Logo">}}

{{< carousel >}}

## Overview

The Scalable Open Architecture for Embedded Edge (SOAFEE) project is an industry-led collaboration defined by automakers, semiconductor suppliers, open source and independent software vendors, and cloud technology leaders. The initiative intends to deliver a cloud-native architecture enhanced for mixed-criticality automotive applications with corresponding open-source reference implementations to enable commercial and non-commercial offerings. 

Building on technologies like [Project Cassini](https://www.arm.com/solutions/infrastructure/edge-computing/project-cassini) and [SystemReady](https://developer.arm.com/architectures/system-architectures/arm-systemready), which define standard boot and security requirements for Arm architecture, SOAFEE adds the cloud-native development and deployment framework while introducing functional safety, security, and real-time capabilities required for automotive workloads.

## Components of SOAFEE

This is a high-level view of an automotive central compute solution stack showing hardware, software, and cloud levels. At the bottom level, standards based firmware and security interfaces ensure system integrators and software developers have a consistent platform enabling seamless secure boot and system bring-up across all compliant hardware.
  
The SOAFEE architecture will seek to re-use existing open standards for the different components in the framework, and will extend those standards as necessary to meet the mixed-criticality requirements of automotive applications.

SOAFEE builds on top of these specifications and standards with a reference framework to standardize key non-differentiating middle-layers, such as the hypervisor, operating systems, container runtime and hardware abstraction layers.  

{{< img src="images/soafee-arch-vision.svg" alt="SOAFEE architectural view" media_page="headless_media">}}

An initial version of SOAFEE, called the SOAFEE R1 is available to download.  Instructions on how to build it are available at the [SOAFEE GitLab repository](https://gitlab.arm.com/soafee/working-groups/-/tree/main/groups/ecosystem/auto_reference_stack). It serves as a starting point to enable automotive DevOps using cloud-native fundamentals.  

## SOAFEE Special Interest Group

The SOAFEE Special Interest Group (SIG) aims to lead and define the cloud-native development paradigm required for a new era of efficient edge workloads. The SIG comprises of the Governing Body, Steering Committees and Working Groups.

### SOAFEE Governing Body

The SOAFEE Governing Body is responsible for defining the strategic direction of the SOAFEE project. To achieve the project goals, the Governing Body will work with the Technical Steering Committee to create working groups and define the goals and objectives for these groups. The Governing Body will validate all output from the working groups to ensure that the strategic objectives of the project are met. 

The Governing Body comprises a group of industry experts to ensure that market intelligence is applied to the output of the SOAFEE project. 

### Governing Body Members

{{< imagegrid columns="3" data="members.governing_body" media_page="headless_media" >}}

### SOAFEE Steering Committees 

The SOAFEE steering committies report directly into the governing body and are responsible for managing the technical and marketing activities in the SOAFEE SIG.

{{< row >}}
{{% column %}}

#### Technical Steering Committee (TSC) 

The role of the Technical Steering Committee is to facilitate communication and collaboration among the Technical Workgroups.   

The TSC is responsible for: 

- Driving SOAFEE technical vision, architecture, and execution 
- Creation of working groups to deliver the strategic vision of the Governing Body 
-  Main coordinating body across workgroups 
- Primary technical interface into Governing Body and MSC 

{{% /column %}}
{{% column %}}

#### Marketing Steering Committee (MSC) 

The role of the Marketing Steering Committee is to drive external communication for the SOAFEE SIG to create market interest and a clear understanding of the purpose and deliverables for SOAFEE.    

The MSC is responsible for : 

- Drive SOAFEE external messaging 
- Create Marketing collateral 
- Plan Marketing events and GTM outreach 
- Work with TSC for Roadmap creation and dissemination 

{{% /column %}}
{{< /row >}}

### SOAFEE Working Groups

The working groups form the core of the ecosystem engagement of the SOAFEE project. Working groups are created under the guidance of the Governing Body and the TSC with the scope, deliverables and sign-off managed by the TSC.

Each working group will be covered by the license and contribution agreement as defined [here](#license).

The working practices of the groups are still being defined, but content from the groups will be captured in the tickets and wiki of this project.

**Working Groups**

The list of currently proposed working groups is:

- Cloud Native Development
- Cloud Native Deployment
- Cloud Native Tooling
- Ecosystem and Reference Implementation

Information on the scope and responsibilities of the working groups will be made available as they become active.

## License

The software is provided under an [MIT license](https://spdx.org/licenses/MIT.html). Contributions to this project are accepted under the same license.

Documentation is provided under the [Creative Commons Attribution 4.0 International License](https://spdx.org/licenses/CC-BY-4.0.html) and all accepted contributions must have the same license.

Contributions
Details of how to make a contribution to the SOAFEE project can be found in the [contributing](https://gitlab.arm.com/soafee/working-groups/-/blob/main/CONTRIBUTING.md) document.

## Resources and Documentation
For more information about the SOAFEE project, explore the links below:

* [White paper: How the SOAFEE architecture brings a cloud-native approach to mixed critical automotive systems](https://armkeil.blob.core.windows.net/developer/Files/pdf/white-paper/arm-scalable-open-architecture-for-embedded-edge-soafee.pdf)
* [Blog: The software-defined vehicle needs hardware that goes the distance](https://www.arm.com/blogs/blueprint/software-defined-vehicle)
* [Blog: The cloud-native approach to the software defined car](https://community.arm.com/2021-ia-reorg-archive/developer/ip-products/system/b/embedded-blog/posts/cloud-native-approach-to-the-software-defined-car?_ga=2.84349662.63983164.1633923986-188627514.1623282169&_gac=1.188151258.1631741153.Cj0KCQjws4aKBhDPARIsAIWH0JWzFX7JhWfY12ecutW_Gaiy3HwXQ1QWT1HbMuvrAnTdtsTSdk57dzAaAq7DEALw_wcB) 
* [SOAFEE on GitLab](https://gitlab.arm.com/soafee)

## Contact

If you would like to learn more or support and contribute to the SOAFEE project, please click [here](mailto:Robert.Day@arm.com)