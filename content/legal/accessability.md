---
Title: Accessability
---

# Accessibility

## Statement
The SOAFEE SIG is committed to providing websites that are accessible to the widest possible audience, regardless of technology or ability. The SOAFEE SIG is actively working to increase the accessibility and usability of soafee.io and associated websites and in doing so adhere to many of the available standards and guidelines.

## Web initiative
This website endeavors to conform to level AA of the World Wide Web Consortium (W3C) Web Content Accessibility Guidelines 2.0. These guidelines explain how to make web content more accessible for people with disabilities. Conformance with these guidelines will help make the web more user friendly for all people.

## W3C standards
This website has been built using code compliant with W3C standards for HTML and CSS. The website displays correctly in current browsers and using standards compliant HTML/CSS code means any future browsers should also display it correctly.  This website also aims to conform to the [W3C Web Web Accessability Initiative](https://www.w3.org/WAI/fundamentals/)

## Exceptions
Whilst the SOAFEE SIG strives to adhere to the accepted guidelines and standards for accessibility and usability, it is not always possible to do so in all areas of this website. We are continually seeking out solutions that will bring all areas of the website up to the same level of overall accessibility. In the meantime should you experience any difficulty in accessing the soafee.io website, please contact us via the [SOAFEE Slack channel](https://soafee.slack.com).
