---
Title: Examples of theme shortcodes and partials
Draft: true
Member: governing_body/arm
Banner:
  Title: This is the banner title
  Description: The banner description.  Should be limited to two lines
  Background: banner/banner1.jpg
Card:
  class: new_member
---
# Examples of theme shortcodes and partials

This page shows all of the shortcodes available in the SOAFEE website.  Shortcodes can be included directly in your markdown, for more information check the [upstream documentation](https://gohugo.io/content-management/shortcodes/).

---

## Member Header

A member header can be used in a page that is sponsored or created by a specific partner.  It adds the partner logo to the left and a markdown formatted content section to the right.

{{< member_header >}}

### Member header

A member header can have markdown formatted content, however currently can not include any shortcodes or partials.
{{< /member_header>}}

---

## Banner

The banner takes data from the page ```frontmatter``` by default.  There is currently no method for overriding the parameters in the shortcode.

```bash
Banner:
  Active: true/false - If true, the banner will show in the carousel
  Title: This is the banner title
  Description: Description text for the banner
  Background: path to the image, relative to the 'headless_media' page
```

{{< banner >}}

---

## Carousel

The carousel picks up on all active banners, it currently doesn't take any parameters

{{< carousel >}}

---

## Image Grid

This shortcode will pull a list of images from a static data file and present in a grid format

{{< table >}}
| parameter  | description |
|----------- |-------------|
| columns | The number of columns to split the grid into.  The grid will create responsive images based on css breakpoints for efficient serving of pages |
| data | the source of the data from .Site.Data |
| media_page | where to source the images from, the default being 'headless_media' |
{{< /table >}}

{{< imagegrid columns="3" data="members.governing_body" media_page="headless_media" >}}

## Img

Create a responsive image based on the backbone CSS breakpoints

{{< table >}}
| parameter  | description |
|----------- |-------------|
| media_page | The source page to get the image from, defaults to the current page |
| src        | the path, relative to media_page, to get the image from |
| alt        | the alt text to apply to the image for compliance with accessability needs |
{{< /table >}}

{{< img media_page="headless_media" src="images/arm.png" alt="Alt text">}}

---

## Question

This shortcode is for formatting a QnA style 

{{< question "This is the text of the question, the answer is in the content and will be revealed below" >}}
This is where the answer goes.  It can include __markdown__.
```bash
# With code examples
In any format
```
However, you can not currently include shortcodes in the markdown section
{{< /question >}}

---

## Table

This shortcode will add styling to the standard markdown table to make it 'pretty'.  Simply place your markdown table as the content block for this shortcode.

{{< table >}}
| parameter  | description |
|----------- |-------------|
| table_class | Override the css class applied to the &lt;table&gt; element |
| thead_class | Override the css class applied to the &lt;thead&gt; element |
{{< /table >}}


{{< table >}}
| Column 1 header | Column 2 header |
|-----------------|-----------------|
| Hello           | World           |
| Markdown is great |
| __SOAFEE__ | Is better |
{{< /table >}}

---
